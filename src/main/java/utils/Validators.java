package utils;

/**
 * 
 * @author paco
 *
 */
public class Validators {

	private static boolean isVacioNulo(String valor) {
		return valor.isEmpty() || valor == null;
	}

	private static boolean isSpaceBlank(String valor) {
		return valor.contains(" ");
	}

}
