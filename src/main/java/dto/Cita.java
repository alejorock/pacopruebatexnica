package dto;

public class Cita {
	private String consultorio;
	private String fecha;
	private String paciente;
	private String doctor;

	public Cita() {
		super();
	}

	public Cita(String consultorio, String fecha, String paciente, String doctor) {
		super();
		this.consultorio = consultorio;
		this.fecha = fecha;
		this.paciente = paciente;
		this.doctor = doctor;
	}

	public String getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(String consultorio) {
		this.consultorio = consultorio;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

}
