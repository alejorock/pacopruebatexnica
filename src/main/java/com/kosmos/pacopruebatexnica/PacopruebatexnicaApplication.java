package com.kosmos.pacopruebatexnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacopruebatexnicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacopruebatexnicaApplication.class, args);
	}

}
