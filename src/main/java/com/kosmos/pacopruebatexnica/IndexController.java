package com.kosmos.pacopruebatexnica;

import java.util.Arrays;
import java.util.List;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import dto.Cita;

@RestController
public class IndexController {

	@RequestMapping("/")
	public ModelAndView index(Model model) {
		Cita cita = new Cita();

		List<String> listConsultorios = Arrays.asList("Consultorio 1", "Consultorio 2", "Consultorio 3");
		List<String> listDoctores = Arrays.asList("Doctor 1", "Doctor 2", "Doctor 3");

		model.addAttribute("listConsultorios", listConsultorios);
		model.addAttribute("listDoctores", listDoctores);
		model.addAttribute("cita", cita);

		ModelAndView mv = new ModelAndView();
		
		mv.setViewName("index/index");

		return mv;
	}

	@RequestMapping("/irCrearCita")
	public ModelAndView irCrearCita(Model model) {
		Cita cita = new Cita();

		// temporal, esto será por bd
		List<String> listConsultorios = Arrays.asList("Consultorio 1", "Consultorio 2", "Consultorio 3");
		List<String> listDoctores = Arrays.asList("Doctor 1", "Doctor 2", "Doctor 3");

		model.addAttribute("listConsultorios", listConsultorios);
		model.addAttribute("listDoctores", listDoctores);
		model.addAttribute("cita", cita);

		ModelAndView mv = new ModelAndView();
		mv.setViewName("modules/citas/alta");

		return mv;
	}

	@RequestMapping("/irConsultaCita")
	public ModelAndView irConsultaCita(Model model) {
		Cita cita = new Cita();

		// temporal, esto será por bd
		List<String> listConsultorios = Arrays.asList("Consultorio 1", "Consultorio 2", "Consultorio 3");
		List<String> listDoctores = Arrays.asList("Doctor 1", "Doctor 2", "Doctor 3");

		model.addAttribute("listConsultorios", listConsultorios);
		model.addAttribute("listDoctores", listDoctores);
		model.addAttribute("cita", cita);

		ModelAndView mv = new ModelAndView();
		mv.setViewName("modules/consultas/consulta");

		return mv;
	}

}
