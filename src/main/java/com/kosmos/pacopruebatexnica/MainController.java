package com.kosmos.pacopruebatexnica;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import dto.Cita;

@RestController
public class MainController {

	@PostMapping("/registrarCita")
	public ModelAndView addCita(@ModelAttribute("cita") Cita cita) {
		System.out.println(cita);

		ModelAndView mv = new ModelAndView();
		mv.setViewName("modules/citas/registro_cita_ok");

		return mv;
	}
	
	
	@PostMapping("/consultarCita")
	public ModelAndView consultarCita(@ModelAttribute("cita") Cita cita) {
		System.out.println(cita);

		ModelAndView mv = new ModelAndView();
		mv.setViewName("modules/consultas/resultado_consulta_cita");

		return mv;
	}
}
